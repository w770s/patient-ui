// 引入全局mixin
import mixin from './libs/mixin/mixin.js'
// 颜色渐变相关,colorGradient-颜色渐变,hexToRgb-十六进制颜色转rgb颜色,rgbToHex-rgb转十六进制
import colorGradient from './libs/function/colorGradient.js'
// 生成全局唯一guid字符串
import guid from './libs/function/guid.js'
// 主题相关颜色,info|success|warning|primary|default|error,此颜色已在uview.scss中定义,但是为js中也能使用,故也定义一份
import color from './libs/function/color.js'
// 添加单位
import addUnit from './libs/function/addUnit.js'

// 规则检验
import test from './libs/function/test.js'
// 去除空格
import trim from './libs/function/trim.js'
// 获取父组件参数
import getParent from './libs/function/getParent.js'
// 获取整个父组件
import $parent from './libs/function/$parent.js'
// 获取sys()和os()工具方法
// 获取设备信息，挂载到$u的sys()(system的缩写)属性中，
// 同时把安卓和ios平台的名称"ios"和"android"挂到$u.os()中，方便取用
import {sys, os} from './libs/function/sys.js'
// 防抖方法
import debounce from './libs/function/debounce.js'
// 节流方法
import throttle from './libs/function/throttle.js'

import OBSupload from './libs/OBS/OBSUploadFile.js'

import * as commonData from './libs/common/version1.js'

// 配置信息
import config from './libs/config/config.js'
// 各个需要fixed的地方的z-index配置文件
import zIndex from './libs/config/zIndex.js'

const $u = {
	colorGradient: colorGradient.colorGradient,
	guid,
	color,
	sys,
	os,
	hexToRgb: colorGradient.hexToRgb,
	rgbToHex: colorGradient.rgbToHex,
	test,
	getParent,
	$parent,
	addUnit,
	trim,
	type: ['primary', 'success', 'error', 'warning', 'info'],
	config, // uView配置信息相关，比如版本号
	zIndex,
	debounce,
	throttle,
	OBSupload,
	commonData
}

const install = Vue => {
	Vue.mixin(mixin)
	Vue.prototype.$u = $u
}

export default {
	install
}
